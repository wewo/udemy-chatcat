module.exports = (passport, FacebookStrategy, config, mongoose) => {

	const chatUser = new mongoose.Schema({
		profileID: String,
		fullname: String,
		profilePicture: String
	});

	const userModel = mongoose.model('chatUser', chatUser);

	passport.serializeUser((user, done) => {
		done(null, user.id);
	});

	passport.deserializeUser((id, done) => {
		userModel.findById(id, (error, user) => {
			done(error, user);
		});
	});

	passport.use(new FacebookStrategy({
			clientID: config.fb.appID,
			clientSecret: config.fb.appSecret,
			callbackURL: config.fb.callbackURL,
			profileFields: ['id', 'displayName', 'photos']
	},
	(accessToken, refreshToken, profile, done) => {
		userModel.findOne({'profileID': profile.id}, (error, result) => {

			if(result) {
				done(null, result);
			}
			else {
				const newChatUser = new userModel({
					profileID: profile.id,
					fullname: profile.displayName,
					profilePicture: profile.photos[0].value || ''
				});

				newChatUser.save((error) => {
					done(null, newChatUser);
				});
			}

		});
	}));

};
