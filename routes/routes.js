'use strict';

module.exports = (express, app, passport) => {
	const router = express.Router();

	function securePages(req, res, next) {
		if(req.isAuthenticated()) {
			next();
		}
		else {
			res.redirect('/');
		}
	}

	router.get('/', (req, res, next) => {
		res.render('index', {title: 'INDEX.'});
	});

	router.get('/auth/facebook', passport.authenticate('facebook'));
	router.get('/auth/facebook/callback', passport.authenticate('facebook', {
		successRedirect: '/chatrooms',
		failureRedirect: '/'
	}));

	router.get('/chatrooms', securePages, (req, res, next) => {
		res.render('chatrooms', {
			title: 'Chatrooms',
			user: req.user
		});
	});

	app.use('/', router);
};
