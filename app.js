'use strict';

const path = require('path');
const express = require('express');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const config = require('./config/config');
const ConnectMongo = require('connect-mongo')(session);
const mongoose = require('mongoose').connect(config.dbURL);
const passport = require('passport');
const FacebookStrategy = require('passport-facebook').Strategy;

const app = express();

app.set('views', path.join(__dirname, '/views'));
app.engine('html', require('hogan-express'));
app.set('view engine', 'html');

app.use(express.static(path.join(__dirname, '/public')));
app.use(cookieParser());

const env = process.env.NODE_ENV || 'development';

if(env === 'development') {
	app.use(session({secret: config.sessionSecret}));
}
else {
	app.use(session({
		secret: config.sessionSecret,
		store: new ConnectMongo({
			mongooseConnection: mongoose.connections[0],
			stringify: true
		})
	}));
}

app.use(passport.initialize());
app.use(passport.session());

require('./auth/passportAuth.js')(passport, FacebookStrategy, config, mongoose);
require('./routes/routes')(express, app, passport);

app.listen(3000, () => {
	console.log('The ChatCAT app is running on port 3000');
	console.log('Mode: ', env);
});
